
const app = require("./index.js");
const chai = require("chai");
const chaiHttp = require("chai-http");
const fs = require('fs');
let should = chai.should();

const { expect } = chai;
chai.use(chaiHttp);

describe("Testovi ruta post i get: ", () => {
   let objekat = {brojVjezbi: 2, brojZadataka: [1,2]}; 
  describe("Testovi za post rutu : ", () => {
        it("Test 1 kreira vjezbe", function(done) {
        chai
          .request("http://localhost:3000")
          .post("/vjezbe")
          .send(objekat)
          .end((err, res) => {
            expect(res.body.status).to.equals("Kreirane vjezbe!");
            done();
          })
        });
        it("Test 1 error", function(done) {
          objekat.brojVjezbi = 10.5;
   objekat.brojZadataka= [1,2];
          chai
            .request("http://localhost:3000")
            .post("/vjezbe")
            .send(objekat)
            .end((err, res) => {
              expect(res.body.data).to.equals("Pogrešan parametar brojVjezbi");
              expect(res.body.status).to.equals("error");
              done();
            })
          });
      
          it("Test 2 error", function(done) {
            objekat.brojVjezbi = -7;
            objekat.brojZadataka= [1,2];
            chai
              .request("http://localhost:3000")
              .post("/vjezbe")
              .send(objekat)
              .end((err, res) => {
                expect(res.body.data).to.equals("Pogrešan parametar brojVjezbi");
                expect(res.body.status).to.equals("error");
                done();
              })
            });

            it("Test 3 error", function(done) {
              
   objekat.brojVjezbi = 18;
   objekat.brojZadataka= [1,2];
                chai
                  .request("http://localhost:3000")
                  .post("/vjezbe")
                  .send(objekat)
                  .end((err, res) => {
                    expect(res.body.data).to.equals("Pogrešan parametar brojVjezbi");
                    expect(res.body.status).to.equals("error");
                    done();
                  })
                });
              
          it("Test 4 error", function(done) {
            objekat.brojVjezbi = 19;
            objekat.brojZadataka= [30,2.1];
                chai
                  .request("http://localhost:3000")
                  .post("/vjezbe")
                  .send(objekat)
                  .end((err, res) => {
                    expect(res.body.data).to.equals("Pogrešan parametar brojVjezbi,z0,z1");
                    expect(res.body.status).to.equals("error");
                    done();
                  })
                });
                it("Test 6 get", function(done) {
                  objekat = {brojVjezbi: 2, brojZadataka: [1,2]}; 
                      chai
                        .request("http://localhost:3000")
                        .get("/vjezbe")
                        .end((err, res) => {
                          expect(JSON.stringify(res.body)).to.equals(JSON.stringify(objekat));
                          done();
                        })
                      });
      });

      
 
      

});
