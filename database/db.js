const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2118684", "root", "password", {
  host: "localhost",
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
}
});
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//import modela u bazu 

db.grupa = require(__dirname + "/model/Grupa.js")(sequelize,Sequelize.DataTypes);
db.student = require(__dirname + "/model/Student.js")(sequelize,Sequelize.DataTypes);
db.vjezba = require(__dirname + "/model/Vjezba.js")(sequelize,Sequelize.DataTypes);


//relacije
//Grupa - jedan na vise -Student
db.grupa.hasMany(db.student,{foreignKey: {allowNull:false}});
  db.student.belongsTo(db.grupa);


module.exports = db; 