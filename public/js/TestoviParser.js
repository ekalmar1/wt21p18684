
let dajTacnost = function  (report) {
    let rezultat ={};
    try {
    let izvjestaj = JSON.parse(report);
    let status = izvjestaj.stats;
   
    let greske =[];
    if (status.pending>0)  {  //testovi se ne mogu izvršiti
        rezultat['tacnost'] = "0%";
        rezultat['greske'] = ["Testovi se ne mogu izvršiti"];
    }
    else if (status.tests==status.passes) { //Svi testovi prlaze
    rezultat['tacnost'] = "100%";
    rezultat['greske'] = greske;
    }
    else{ //Ostale mogućnosti
        let tacnost =(status.passes/status.tests) * 100;
        if (Number.isInteger(tacnost)) tacnost = tacnost + "%";
        else {
            tacnost = Math.round(tacnost * 10) / 10  + "%" ;
        }
        rezultat['tacnost'] = tacnost;
        greske=izvjestaj.failures;
        let fullTitles = [];
        for (let i=0;i<greske.length; i++) {
            let greska = {};
            greska = greske[i];
            fullTitles[i] = greska['fullTitle'];
        }
        rezultat['greske'] = fullTitles;
        
    }
}
catch (err) {
    rezultat['tacnost'] = "0%";
    rezultat['greske'] = ["Testovi se ne mogu izvršiti"];
}
        return rezultat;
    }
    
    
    
    
     let porediRezultate =function  (report1, report2) {
        let izvjesaj1= JSON.parse(report1);
        let status1 = izvjesaj1.stats;
        let testovi1 = izvjesaj1.tests;
        let izvjesaj2= JSON.parse(report2);
        let status2 = izvjesaj2.stats;
        let testovi2 = izvjesaj2.tests;
        let rezultat ={};
        let br = 0;
        br = brojJednakihTestova(testovi1,testovi2);
        if (testovi1.length==testovi2.length && br==testovi1.length && status1.passes==status2.passes && status1.failures==status2.failures) {
            let promjena = dajTacnost(report2);
            let greske = dajTacnost(report1);
         rezultat['promjena'] = promjena['tacnost'];
         rezultat['greske'] = greske['greske'];
        } 
    else {
       
        rezultat['promjena'] =  (Math.round(((padaju(izvjesaj1.failures,testovi2) + (status2.failures))/(padaju(izvjesaj1.failures,testovi2)+(status2.tests)) * 100) * 10) / 10)+"%";
        rezultat['greske'] =  padajuINePonavljajuSe(izvjesaj1.failures,testovi2,izvjesaj2.failures);
    }
    return rezultat;
    
    }
    
    function padaju (greske,testovi) {
        let brojac=0;
        let brKojiPadaju =0;
        for (let i =0; i<greske.length;i++) {
            for (let j=0;j<testovi.length;j++) {     
                if (JSON.stringify(greske[i]) == JSON.stringify(testovi[j])) {
                    brojac++;
                    }
            }
            if (brojac==0) brKojiPadaju++;
        }
        return brKojiPadaju;
    }
    
    
    function brojJednakihTestova (testovi1,testovi2) {
        let brojac=0;
        for (let i =0; i<testovi1.length;i++) {
            for (let j=0;j<testovi2.length;j++) {     
                if (JSON.stringify(testovi1[i]) == JSON.stringify(testovi2[j])) {
                    brojac++;
                    }
            }
        }
        
        return brojac;
    }
    function sortiraj(array) {
        for (let i = 0; i < array.length; i++) {
            for (let j = 0; j < array.length; j++) {
                if (String(array[j]).toLowerCase() > String(array[j + 1]).toLowerCase()) {
                    let temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }
    function padajuINePonavljajuSe (greske1,tests,greske2) {
        let jedankiSU=false;
        let fullTitles1 = [];
        let pozicijaUNizu=0;
        //prolzimo petljom i trazimo jednake
        for (let i =0; i<greske1.length;i++) {
            for (let j=0;j<tests.length;j++) {     
                if (JSON.stringify(greske1[i]) === JSON.stringify(tests[j])) {
                    jedankiSU=true;
                    }
            }
            if (jedankiSU==false) {
                //u niz smjestimo sve one koji padaju u prvom reportu a ne pojavljuju se u drugom
                fullTitles1[pozicijaUNizu]=greske1[i].fullTitle;
                pozicijaUNizu++;
            }
            jedankiSU=false;
        }
        //niz sortiramo
       fullTitles1= sortiraj(fullTitles1);
        
        let fullTitles2 = [];
      //u niz smjestimo sve one koji padaju u drugom
        for (let i=0;i<greske2.length;i++) {
            fullTitles2[i]=greske2[i].fullTitle;
        }
        //drugi dio niza sortiramo
        fullTitles2= sortiraj(fullTitles2);
        
       //spojimo prvi dio sa drugim
        for (let i=0;i<fullTitles2.length;i++) {
            fullTitles1[pozicijaUNizu]=fullTitles2[i];
            pozicijaUNizu++;
        }
        return fullTitles1;
    
    }
  
    
    
    