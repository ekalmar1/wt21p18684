
let VjezbeAjax = (function () {
var dodajInputPolja = function (div,brojVjezbi) {
    for (let i=0;i<brojVjezbi;i++) {
        let vjezba = document.createElement("label");
        let unos = document.createElement("input");
        let tekst = document.createTextNode("VJEZBA " + (i+1));
      //  let noviRed  =document.createElement("br");
        vjezba.appendChild(tekst);
        div.appendChild(vjezba);
        //div.appendChild(noviRed);
        div.appendChild(unos);
       // div.appendChild(noviRed);
    }


    

}
var posaljiPodatke = function (div,callback) {
  
    let chld =div.children;
    let brojZadataka = [];
    let j=0;
    let brojVjezbi = chld.length/2;
    for (let i=0;i<chld.length;i++){
        if (i%2==1) {
        brojZadataka[j]= (chld[i]).value;
        j++;
       
        }
    }
    let objekat = {};
    objekat.brojVjezbi = brojVjezbi;
    objekat.brojZadataka = brojZadataka;
   
    $.ajax({
        
        
        url: 'http://localhost:3000/vjezbe' ,
        type: 'POST',
        dataType: "json",
        data: objekat,
       // contentType: 'application/json; charset=utf-8',
        success: function (result) {
            callback(null,result);
         },
         failure: function (errMsg) {
            callback(errMsg,null);
         }});  
 
         
}

var dohvatiPodatke = function (callback) {
    
    $.ajax({
        
        type: 'GET',
    
        url: 'http://localhost:3000/vjezbe' ,
        
        
      // contentType: 'application/json; charset=utf-8',
       success: function (data) {
           
        callback(null,data);
      },
         failure: function (errMsg) {
           
             callback(errMsg,null);
         }});  

}

var iscrtajVjezbe = function (div,objekat) {
    let vjezbe = JSON.parse(objekat);
    let brojVjezbi=vjezbe.brojVjezbi;

    for (let i=0;i<brojVjezbi;i++) {
        
        let vjezba = document.createElement("div");
        vjezba.setAttribute("class","div");
        let dugme = document.createElement("button");
        dugme.setAttribute("class","dugme");
        let tekst = document.createTextNode("VJEZBA " + (i+1));
        dugme.appendChild(tekst);
        
        vjezba.appendChild(dugme);
        div.appendChild(vjezba);
       
        let divZadaci = document.createElement("div");
        divZadaci.setAttribute("class","dugme1");
       
        iscrtajZadatke(divZadaci,(vjezbe.brojZadataka)[i]);
        div.appendChild(divZadaci);
        dugme.onclick = function () {
       
            if (divZadaci.getAttribute("class")=="dugme1") {
                divZadaci.setAttribute("class","dugme2");
            }
            else if (divZadaci.getAttribute("class")=="dugme2") {
                divZadaci.setAttribute("class","dugme1");
            }
        } 
    }
}

var iscrtajZadatke = function (div,brojZadataka) {
 
    for (let i=0;i<brojZadataka;i++) {
        let dugme = document.createElement("button");
        
        let tekst = document.createTextNode("Zadatak " + (i+1));
        dugme.appendChild(tekst);
        div.appendChild(dugme);
       
    }
   
}

var dodajBatch= function (csv,callbackFJA){

        alert(csv);
    $.ajax({
        type: "POST",
        enctype: 'text/csv',
        url: 'http://localhost:3000/batch/student',
        data: csv,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
            alert("hrere")
            callbackFJA(data,null);
        },
        failure: function (error) {
            callbackFJA(null,error);
        }
    });
}

var ispisiOdg = function (div,odg){
    alert(odg);
    let label = documenz.createElement("label");
    let text = document.createTextNode(JSON.stringify(odg));
    label.appendChild(text);
         div.appendChild(label);
}

var posaljiStudent= function (studentObjekat, callback){

    
    $.ajax({
        url: 'http://localhost:3000/student',
        type: 'POST',
        data: studentObjekat,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            callback(result,null);
        },
        failure: function (errMsg) {
            callback(null,errMsg);
        }
    });
}

var postaviGrupu=function (indexStudenta,grupa,callback){

    
    $.ajax({
        url: 'http://localhost:3000/student/'+indexStudenta,
        data:grupa,
        type: 'PUT',
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            callback(result,null);
        },
        failure: function (errMsg) {
            callback(null,errMsg);
        }
    });
}

    return {
dodajInputPolja:dodajInputPolja,
posaljiPodatke: posaljiPodatke,
dohvatiPodatke: dohvatiPodatke,
iscrtajVjezbe:iscrtajVjezbe,
iscrtajZadatke:iscrtajZadatke,
dodajBatch:dodajBatch,
ispisiOdg: ispisiOdg,
posaljiStudent :posaljiStudent,
postaviGrupu :postaviGrupu
    }
}());