var express = require('express');
var app = express();
var path = require('path');
const bodyParser = require("body-parser");
const fs = require("fs");
const { parse } = require("path");
const { json } = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());
app.use(
    express.raw({
      inflate: true,
      limit: '50mb',
      type: () => true, // this matches all content types
    })
  );


  const db = require("./database/db.js");
db.sequelize.sync({
    force: true
   });
app.use(express.static(path.join(__dirname, 'public')));
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + "/public/grupe.html"));
});
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + "/public/vjezbe.html"));
});
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + "/public/zadaci.html"));
});
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + "/public/mojRepozitorij.html"));
});
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + "/public/zadatak2.html"));
});
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + "/public/zadatak4.html"));
});
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + "/public/unosVjezbi.html"));
});


// post vjezbe 


app.post('/vjezbe',function(req,res){
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true);
    let tijelo1 = req.body; 
    let tijeloStr = JSON.stringify(tijelo1);
    let tijelo = JSON.parse(tijeloStr);
   
    let brVjezbi = tijelo.brojVjezbi;  
    let brZadataka = tijelo.brojZadataka;
    
    let odgovor="";
    let kraj = false;
    if (brVjezbi<1 || brVjezbi>15 || !Number.isInteger(Number(brVjezbi))){
        kraj=true;
        odgovor+="Pogrešan parametar brojVjezbi"
        let brojeviZ ="";
        for (let i=0;i<brZadataka.length;i++){
            if (brZadataka[i]<0 || brZadataka[i]>10 || !Number.isInteger(Number(brZadataka[i]))) {
                
                brojeviZ+=",z"+ i ;
            }

        }
        odgovor+=brojeviZ;
        res.json({status:"error",data:odgovor});
    }
    if(!kraj) {
    let greska = false;
    let odgovor2="Pogresan parametar ";
    for (let i=0;i<brZadataka.length;i++){
        if (brZadataka[i]<0 || brZadataka[i]>10 || !Number.isInteger(Number(brZadataka[i]))) {
           
          greska=true;
          if (i==brZadataka.length-1)
            odgovor2+="z"+i;
            else odgovor2+="z"+i+",";
        }
        if (greska)  { res.json({status:'error',data:odgovor2});
        return;
    }
    }
   
    for (let i=0;i<brVjezbi;i++){
        db.vjezba.create({ brojVjezbi : (i+1),
            brojZadataka: brZadataka[i]
    }).then(odgovor =>{res.end("Vjezbe su dodane")
    }).catch(function (err) {
          console.log( err);
          return 0;
      });
    
     }
}
});



//get vjezbe

app.get("/vjezbe", (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true);
   
    db.vjezba.findAll().then(function(vjezbe) {
      let sveVjezbe = [];
      vjezbe.forEach(vjezba => {
        sveVjezbe.push(vjezba);
      });
      let vjezbeObjekat = {brojVjezbi : sveVjezbe.length, brojZadataka: []};
      
    let brZadatakaZaVjezbu=  [];
      for (let i=0;i<sveVjezbe.length;i++){
          brZadatakaZaVjezbu[i]=sveVjezbe[i].brojZadataka;
      }
      vjezbeObjekat.brojZadataka = brZadatakaZaVjezbu;
      res.send(JSON.stringify(vjezbeObjekat));
    });
  });

//post studente iz csv
app.post('/batch/student',function(req,res){
    let body = req.body.toString(); 

    let studenti = body.split("\n");
    let studentiIzCSV = [];
    for (let i =0;i<studenti.length;i++){
    let student = {};
    
        student.ime = studenti[i].split(",")[0];
        student.prezime=studenti[i].split(",")[1];
        student.index=studenti[i].split(",")[2];
        student.grupa=studenti[i].split(",")[3];
    studentiIzCSV[i] = student;  
    }

    let brojNovihStudenata = 0;
    let resData = "";

    for (let i=0;i<studentiIzCSV.length;i++) {
   let id =1;
db.student.findOne({ where: { index: studentiIzCSV[i].index}}).then(function(student) {
    if (student!=null) {
        if (i==studentiIzCSV.length -1) resData+= studentiIzCSV[i].index;
        else 
        resData+= studentiIzCSV[i].index + ",";
        if (i==studentiIzCSV.length-1) {
            let status ="";
            status = "Studenti {" + resData + "} već postoje!";
         let response = {};
          response.stutus = status;
         res.json(response);
        }
    }

    else {
        db.grupa.findOne({where: {naziv : studentiIzCSV[i].grupa}}).then(function(responseGl){
            if (responseGl==null){
                db.grupa.create({ naziv : studentiIzCSV[i].grupa
            }).then( function(response1) {
                id = (JSON.parse(JSON.stringify(response1))).id;
                db.student.findOne({where: {index: studentiIzCSV[i].index}}).then (function(responseStd){
                    if (responseStd==null) {
                        let novi = {
                            ime: studentiIzCSV[i].ime,
                            prezime : studentiIzCSV[i].prezime,
                            index : studentiIzCSV[i].index,
                            GrupaId : id
                        }
                        db.student.create(novi).then(function(response3) {
                            if(i!=studentiIzCSV.length-1) brojNovihStudenata++; 
                            else {
                                brojNovihStudenata++;
                                let status = "";
   
                                if (brojNovihStudenata< studentiIzCSV.length) {
                                    status = "Dodano {" + brojNovihStudenata + "} studenata, a studenti {" + resData + "} već postoje!";
                                    res.send({status: status});
                                }
                                else {
                                    status = "Dodano {" + brojNovihStudenata + "} studenata";
                                    res.send({status: status});
                                }
                            }
                        
                        }).catch(function(error) {
                            console.log(error);
                            return 0;
                        })
                    }
                    
                })
            }).catch(function (error) {
                  console.log( error);
                  return 0;
              }); 
             
            }
            else {
            id = (JSON.parse(JSON.stringify(responseGl))).id;
            db.student.findOne({where: {index: studentiIzCSV[i].index}}).then (function(responseStd){
                if (responseStd==null) {
                  let novi = {
                    ime: studentiIzCSV[i].ime,
                    prezime : studentiIzCSV[i].prezime,
                    index : studentiIzCSV[i].index,
                    GrupaId : id
                }
                    db.student.create(novi).then(function(odg3) {
                        if(i!=studentiIzCSV.length-1) brojNovihStudenata++; 
                        else {
                            brojNovihStudenata++;
                            let status = "";

                            if (brojNovihStudenata< studentiIzCSV.length) {
                                status = "Dodano {" + brojNovihStudenata + "} studenata, a studenti {" + resData + "} već postoje!";
                                res.send({status: status});
                            }
                            else {
                                status = "Dodano {" + brojNovihStudenata + "} studenata";
                                res.send({status: status});
                            }
                        }
                    
                    }).catch(function(error) {
                        console.log(error);
                        return 0;
                    })
                }
                
            })
            }
    
        }).catch(function (error) {
            console.log( error);
            return 0;
        }); 
       
       
     

    }
}).catch(function (err) {
    console.log( err);
    return 0;
}); 


    }
    
      
  });


app.listen(3000);




